package vn.titv.spring.demo.restTemplate;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import vn.titv.spring.demo.com.Post;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/api")
public class ApiController {

    private final String apiUrl = "https://jsonplaceholder.typicode.com/posts";
    private final RestTemplate restTemplate;
    private final Gson gson;

    @Autowired
    public ApiController(RestTemplate restTemplate, Gson gson) {
        this.restTemplate = restTemplate;
        this.gson = gson;
    }

    @GetMapping("/get-all-data")
    public List<Post> getAllDataFromApi() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(apiUrl, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            String json = responseEntity.getBody();
            // Sử dụng gson để chuyển đổi JSON thành class object
            List<Post> posts = gson.fromJson(json, new TypeToken<List<Post>>(){}.getType());
            return posts;
        } else {

            return Collections.emptyList();
        }
    }
}










